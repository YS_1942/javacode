import java.util.ArrayList;
import java.util.List;

/**
 * 树中两个结点的最低公共祖先
 * */
class TreeNode1{
	int val;
	List<TreeNode1> children = new ArrayList<>();
	TreeNode1 left;
	TreeNode1 right;
	public TreeNode1(int val){
		this.val = val;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.val+"";
	}
}
public class Main50 {
	public static TreeNode1 getLastCommonParent(TreeNode1 root,TreeNode1 p1,TreeNode1 p2){
		List<TreeNode1> path1 = new ArrayList<>();
		List<TreeNode1> path2 = new ArrayList<>();
		
		List<TreeNode1> tempList = new ArrayList<>();
		
		getNodePath(root, p1, tempList, path1);
		getNodePath(root, p2, tempList, path2);
		
		if(path1.size() == 0 || path2.size() == 0)
			return null;
		
		return getLastCommonParent(path1, path2);
	}
	public static void getNodePath(TreeNode1 root,TreeNode1 target,List<TreeNode1> tempList,List<TreeNode1> path){
		if(root == null || root == target){
			return;
		}
		tempList.add(root);
		List<TreeNode1> children = root.children;
		for (TreeNode1 node : children) {
			if(node == target){
				path.addAll(tempList);
				break;
			}
			getNodePath(root, target, tempList, path);
		}
		tempList.remove(tempList.size()-1);
	}
	public static TreeNode1 getLastCommonParent(List<TreeNode1> p1,List<TreeNode1> p2){
		TreeNode1 tempNode = null;
		for (int i = 0; i < p1.size(); i++) {
			if(p1.get(i) != p2.get(i)){
				return tempNode;
			}
			tempNode = p1.get(i);
		}
		return tempNode;
	}
	
	//二叉搜索树
	public static TreeNode1 getLastCommonNode2(TreeNode1 root,TreeNode1 node1,TreeNode1 node2){
		if(root == null) return null;
		if(root.val >= node1.val && root.val <= node2.val)
			return root;
		else if(root.val < node1.val)
			return getLastCommonNode2(root.right, node1, node2);
		else
			return getLastCommonNode2(root.left, node1, node2);
	}
}

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * 给定一个数组和滑动窗口的大小，找出所有滑动窗口里数值的最大值。
 * */
public class Main65 {
	public ArrayList<Integer> maxInWindows(int [] num, int size){
        if(num.length == 0 || size < 1 ||num.length<size)
        	return new ArrayList<>();
        ArrayList<Integer> result = new ArrayList<>();
        LinkedList<Integer> qmax = new LinkedList<>();
        for (int i = 0; i < num.length; i++) {
			while(!qmax.isEmpty() && num[qmax.peekLast()]<num[i])
				qmax.pollLast();
			qmax.addLast(i);
			if(qmax.peekFirst() == i-size)
				qmax.pollFirst();
			if(i>=size-1)
				result.add(num[qmax.peekFirst()]);
		}
        return result;
    }
}

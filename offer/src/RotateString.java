/**
 * ����ת�ַ���
 * */
public class RotateString {
//	public String LeftRotateString(String str,int n) {
//		if(n>=str.length())
//			return str;
//        String result = "";
//        for (int i = n; i < str.length(); i++) {
//			result+=str.charAt(i);
//		}
//        for (int i = 0; i < n; i++) {
//			result+=str.charAt(i);
//		}
//        return result;
//    }
	public String LeftRotateString(String str,int n) {
		char[] ch = str.toCharArray();
		swap(ch, 0, n-1);
		swap(ch, n, str.length()-1);
		swap(ch, 0, str.length()-1);
		return new String(ch);
	}
	public void swap(char[] ch,int i,int j){
		char tmp;
		while(i<j){
			tmp = ch[i];
			ch[i] = ch[j];
			ch[j] = tmp;
			i++;
			j--;
		}
	}
}

import java.util.Arrays;

public class Check {
	public static void swap(int[]a,int i,int j){
		int temp = a[i];
		a[i] = a[j];
		a[j] = temp;
	}
	public static void quickSort(int[] a){
		qSort(a,0,a.length-1);
	}
	public static void qSort(int[] a,int low,int high){
		if(low<high){
			int pivot = partition(a,low,high);
			qSort(a, low, pivot-1);
			qSort(a, pivot+1, high);
		}
	}
	public static int partition(int[] a,int low,int high){
		int pivotkey = a[low];
		while(low<high){
			while(low<high && a[high]>=pivotkey)
				high--;
			swap(a, low, high);
			while(low<high && a[low]<=pivotkey)
				low++;
			swap(a, low, high);
		}
		return low;
	}
	public static void main(String[] args) {
//    	int[] a = {0,9,8,7,6,5,4,3,2,1,0};
//    	int[] a = {49,38,65,97,76,13,27,49,55,4};
    	int[] a = {50,10,90,30,70,40,80,60,20};
    	quickSort(a);
    	System.out.println(Arrays.toString(a));
	}
}
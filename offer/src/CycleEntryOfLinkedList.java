import java.util.HashSet;

/**
 * 给一个链表，若其中包含环，请找出该链表的环的入口结点，否则，输出null。
 * */
public class CycleEntryOfLinkedList {
//	public ListNode EntryNodeOfLoop(ListNode pHead){
//		HashSet<ListNode> set = new HashSet<>();
//		while(pHead != null){
//			if(set.contains(pHead))
//			//if(!set.add(pHead))
//				return pHead;
//			else{
//				set.add(pHead);
//				pHead = pHead.next;
//			}
//		}
//		return null;
//    }
	public ListNode EntryNodeOfLoop(ListNode pHead){
		ListNode slow = pHead;
		ListNode fast = pHead;
		while(fast != null && fast.next != null){
			fast = fast.next.next;
			slow = slow.next;
			if(fast == slow)
				break;
		}
		if(fast == null || fast.next == null)
			return null;
		fast = pHead;
		while(fast != slow){
			fast = fast.next;
			slow = slow.next;
		}
		return slow;
	}
}

import java.util.HashSet;
import java.util.Set;

/**
 * 数组中重复的数字
 * */
public class RepeateNumsInArray {
	//方法一
//	public boolean duplicate(int numbers[],int length,int [] duplication) {
//	    boolean[] k = new boolean[length];
//	    for (int i = 0; i < length; i++) {
//			if(k[numbers[i]]){
//				duplication[0] = numbers[i];
//				return true;
//			}
//			k[numbers[i]] = true;
//		}
//	    return false;
//    }
	public boolean duplicate(int numbers[],int length,int [] duplication) {
		Set<Integer> set = new HashSet<>();
		for (int i = 0; i < length; i++) {
			if(!set.contains(numbers[i])){
				set.add(numbers[i]);
			}
			else{
				duplication[0] = numbers[i];
				return true;
			}
		}
		return false;
	}
	//方法二
	public boolean duplicate1(int numbers[],int length,int [] duplication) {
	    for (int i = 0; i < length; i++) {
			int index = numbers[i];
			if(index >= length)
				index -= length;
			if(numbers[index] >= length){
				duplication[0] = index;
				return true;
			}
			numbers[index] = numbers[index] + length;
		}
	    return false;
    }
	
}

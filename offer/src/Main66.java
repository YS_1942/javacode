/**
 * 请设计一个函数，用来判断在一个矩阵中是否存在一条包含某字符串所有字符的路径。
 * 路径可以从矩阵中的任意一个格子开始，每一步可以在矩阵中向左，向右，向上，向下移动一个格子。
 * 如果一条路径经过了矩阵中的某一个格子，则之后不能再次进入这个格子
 * */
public class Main66 {
	public boolean hasPath(char[] matrix, int rows, int cols, char[] str){
		boolean[] flag = new boolean[matrix.length];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				if(match(matrix, rows, cols, i, j, str, 0, flag))
					return true;
			}
		}
		return false;
    }
	private boolean match(char[] matrix, int rows,int cols,int i,int j,char[] str,int k,boolean[] flag){
		int index = i*cols + j;
		if(i < 0 || i >= rows || j<0 ||j >=cols || matrix[index] != str[k] || flag[index] == true)
			return false;
		if(k == str.length -1) return true;
		flag[index] = true;
		if(match(matrix, rows, cols, i-1, j, str, k+1, flag)
			||match(matrix, rows, cols, i+1, j, str, k+1, flag)
			||match(matrix, rows, cols, i, j-1, str, k+1, flag)
			||match(matrix, rows, cols, i, j+1, str, k+1, flag))
			return true;
		flag[index] = false;
		return false;
	}
}

/**
 * 请实现两个函数，分别用来序列化和反序列化二叉树
 * */
public class Main62 {
	public int index = -1;
	String Serialize(TreeNode root) {
		StringBuffer sb = new StringBuffer();
		if(root == null){
			sb.append("#,");
			return sb.toString();
		}	
		sb.append(root.val + ",");
		sb.append(Serialize(root.left));
		sb.append(Serialize(root.right));
		return sb.toString();
	}
	TreeNode Deserialize(String str) {
		index++;
		String[] string = str.split(",");
		int len = string.length;
		if(index >= len)
			return null;
		TreeNode node = null;
		if(!string[index].equals("#")){
			node = new TreeNode(Integer.valueOf(string[index]));
			node.left = Deserialize(str);
			node.right = Deserialize(str);
		}
		return node;
	}
}

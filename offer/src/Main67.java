/**
 *地上有一个m行和n列的方格。一个机器人从坐标0,0的格子开始移动，
 *每一次只能向左，右，上，下四个方向移动一格，
 *但是不能进入行坐标和列坐标的数位之和大于k的格子。 
 *请问该机器人能够达到多少个格子？
 */
public class Main67 {
	int sum = 0;
	public int movingCount(int threshold, int rows, int cols){
		boolean[] flag = new boolean[rows*cols];
		match(threshold, rows, cols, 0, 0, flag);
        return sum;
    }
	private void match(int threshold,int rows,int cols,int i,int j,boolean[] flag){
		int index = i*cols + j;
		if(i<0 || i>= rows || j<0||j>=cols || bigger(threshold, i, j)||flag[index]==true)
			return;
		sum++;
		flag[index] = true;
		if(i == rows -1 &&j == cols-1) return;
		match(threshold, rows, cols, i+1, j, flag);
		match(threshold, rows, cols, i, j+1, flag);
	}
	private boolean bigger(int threshold,int i,int j){
		int tmp=0;
		while(i != 0){
			tmp += (i%10);
			i /= 10;
		}
		while(j != 0){
			tmp = tmp +(j%10);
			j /= 10;
		}
		if(tmp > threshold)
			return true;
		return false;
	}
}

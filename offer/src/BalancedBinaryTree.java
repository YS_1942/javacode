/**
 * ����һ�ö��������жϸö������Ƿ���ƽ���������
 * */
public class BalancedBinaryTree {
	public boolean IsBalanced_Solution(TreeNode root) {
        return getDepth(root) != -1;
    }
	public int getDepth(TreeNode node){
		if(node == null)
			return 0;
		int left = getDepth(node.left);
		if(left == -1)
			return -1;
		int right = getDepth(node.right);
		if(right == -1)
			return -1;
		return Math.abs(left-right) >1? -1:1+Math.max(left, right);
	}
}

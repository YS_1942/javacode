/**
 * 输入一个链表，反转链表后，输出新链表的表头。
 * */

public class ReverseLinkedList {
	/**不用递归*/
	public ListNode ReverseList1(ListNode head) {
		if(head == null)
			return null;
		ListNode p = head.next;
		ListNode temp = null;
		ListNode q = head;
		q.next = null;
		while(p != null){
			temp = p.next;
			p.next = q;
			q = p;
			p = temp;
		}
		return q;
    }
	/**用递归*/
	public ListNode ReverseList(ListNode head) {
		if(head == null)
			return null;
		ListNode p = head.next;
		ListNode q = head;
		q.next = null;
		return re(p, q);
    }
	public static ListNode re(ListNode p,ListNode q){
		if(p != null){
			ListNode temp = p.next;
			p.next = q;
			return re(temp, p);
		}
		else
			return q;
	}
	/**反转链表的m~n位*/
	public ListNode reverseBetween(ListNode head, int m, int n) {
        ListNode dummy = new ListNode(0);
        dummy.next = head;
        ListNode preStart = dummy;
        ListNode start = head;
        for (int i = 1; i < m; i++) {
			preStart = start;
			start = start.next;
		}
        for (int i = 0; i < n-m; i++) {
			ListNode tmp = start.next;
			start.next = tmp.next;
			tmp.next = preStart.next;
			preStart.next = tmp;
		}
        return dummy.next;
    }
}

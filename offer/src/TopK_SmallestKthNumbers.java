import java.util.ArrayList;
/**TopK问题
 * 输入n个整数，找出其中最小的K个数。
 * 例如输入4,5,1,6,2,7,3,8这8个数字，则最小的4个数字是1,2,3,4,。
 * */
public class TopK_SmallestKthNumbers {
//	public ArrayList<Integer> GetLeastNumbers_Solution(int [] input, int k) {
//		ArrayList<Integer> result = new ArrayList<>();
//		int len = input.length;
//		if(input == null || len == 0 || len <k)
//			return result;
//		for (int i = k/2-1; i >= 0; i--) {
//			heapAdjust(input,i,k-1);
//		}
//		for (int i = k; i < len; i++) {
//			if(input[i] < input[0]){
//				swap(input, 0, i);
//				heapAdjust(input, 0, k-1);
//			}
//		}
//		for (int i = 0; i < k; i++) {
//			result.add(input[i]);
//		}
//		return result;
//	}
//	public void heapAdjust(int[] input,int s,int n){
//		int tmp = input[s];
//		for (int i = 2*s+1; i <= n; i = 2*i+1) {
//			if(i<n && input[i]<input[i+1])
//				i++;
//			if(tmp >= input[i])
//				break;
//			input[s] = input[i];
//			s = i;
//		}
//		input[s] = tmp;
//	}
//	public static void swap(int[] ch, int i, int j){
//		int temp = ch[i];
//		ch[i] = ch[j];
//		ch[j] = temp;
//	}
	public void swap(int[] a,int i,int j){
		int tmp = a[i];
		a[i] = a[j];
		a[j] = tmp;
	}
	public void heapAdjust(int[] a,int s,int m){
		int tmp = a[s];
		for (int i = 2*s+1; i <=m ; i=2*i+1) {
			if(i<m&&a[i]<a[i+1])
				i++;
			if(tmp>=a[i])
				break;
			a[s] = a[i];
			s = i;
		}
		a[s] = tmp;
	}
	public ArrayList<Integer> GetLeastNumbers_Solution(int [] input, int k) {
		ArrayList<Integer> result = new ArrayList<>();
		int len = input.length;
		if(input == null || len == 0 || len<k)
			return result;
		for (int i = k/2-1; i >=0 ; i--) {
			heapAdjust(input, i, k-1);
		}
		for (int i = k; i <len; i++) {
			if(input[i] < input[0]){
				swap(input, i, 0);
				heapAdjust(input, 0, k-1);
			}
		}
		for (int i = 0; i < k; i++) {
			result.add(input[i]);
		}
		return result;
	}
}

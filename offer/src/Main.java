import java.util.Arrays;
import java.util.Scanner;

//import java.util.ArrayList;
//
//import org.omg.CORBA.FREE_MEM;
//
//
//public class Main {
//	/**头插法创建链表*/
//	public static ListNode headCreate(){
//		ListNode listNode = new ListNode(1);
//		for (int i = 0; i < 5; i++) {
//			ListNode lN = new ListNode(i+1);
//			lN.next = listNode.next;
//			listNode.next = lN;
//		}
//		return listNode;
//	}
//	/**尾插法创建链表*/
//	public static ListNode endCreate(){
//		ListNode listNode = new ListNode(1);
//		ListNode r = listNode;
//		for (int i = 0; i < 5; i++) {
//			ListNode p = new ListNode(i+1);
//			r.next = p;
//			r = p;
//		}
//		r.next = null;
//		return listNode;
//	}
//	/**打印链表*/
//	public static void printNode(ListNode listNode){
//		while(listNode != null){
//			System.out.print(listNode.val+" ");
//			listNode = listNode.next;
//		}
//		System.out.println();
//		
//	}
//	/**单链表的整表删除 */
//	public static void deleteList(ListNode listNode){
//		ListNode p = listNode;
//		ListNode q = null;
//		while(listNode != null){
//			q = listNode.next;
//			listNode.next = null;
//			listNode = q;
//		}
//		listNode = p;
//	}
//	public static void main(String[] args) {
//		System.out.println("头插法创建链表：");
//		ListNode headList = headCreate();
//		printNode(headList);
//		System.out.println("整表删除后：");
//		ListNode r = headList.next;
//		deleteList(headList);
//		
//		printNode(headList);
//		System.out.println("尾插法创建链表：");
//		ListNode endList = endCreate();
//		printNode(endList);
//	}
//}



public class Main {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int n = input.nextInt();
		int[] result = new int[n];
		for (int i = 0; i < n; i++) {
			int m = input.nextInt();
			if(m ==1)
				result[i] = 1;
			else{
				int[] a = new int[m];
				for (int j = 0; j < m; j++) {
					a[j] = input.nextInt();
				}
				int[] tmp = new int[m];
				Arrays.fill(tmp, 1);
				for (int j = 1; j < m; j++) {
					if(a[j] > a[j-1])
						tmp[j] = tmp[j-1] +1;
				}
				int sum = tmp[m-1];
				for (int j = m-2; j >= 0; j--) {
					if(a[j]>a[j+1] && tmp[j] <= tmp[j+1])
						tmp[j] = tmp[j+1] +1;
					sum += tmp[j];
				}
				result[i] = sum;
			}
		}
		for (int i : result) {
			System.out.println(i);
		}
	}
}







/**
 * 操作给定的二叉树，将其变换为源二叉树的镜像。
 * */
public class MirrorOfBinaryTree {
	public void Mirror(TreeNode root) {
        if(root == null)
        	return;
        if(root.left == null && root.right == null)
        	return;
        TreeNode temp = root.left;
        root.left = root.right;
        root.right = temp;
        Mirror(root.left);
        Mirror(root.right);
    }
	//判断树是不是镜像的
	public boolean isSymmetric(TreeNode root) {
		if(root == null)
			return true;
		return help(root.left, root.right);
	}
	public boolean help(TreeNode left,TreeNode right) {
		if(left == null && right == null)
			return true;
		if(left == null || right == null)
			return false;
		if(left.val != right.val){
			return false;
		}
		return help(left.left, right.right) && help(left.right, right.left);
	}
}

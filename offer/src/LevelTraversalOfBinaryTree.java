import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

/**
 * 从上到下按层打印二叉树，同一层结点从左至右输出。每一层输出一行。
 * */
public class LevelTraversalOfBinaryTree {
	ArrayList<ArrayList<Integer> > Print(TreeNode pRoot) {
		ArrayList<ArrayList<Integer>> result = new ArrayList<>();
		if(pRoot == null)
			return result;
		Queue<TreeNode> layer = new LinkedList<>();
		ArrayList<Integer> layerList = new ArrayList<>();
		layer.add(pRoot);
		int start = 0,end = 1;
		while(!layer.isEmpty()){
			TreeNode temp = layer.remove();
			layerList.add(temp.val);
			start++;
			if(temp.left != null)
				layer.add(temp.left);
			if(temp.right != null)
				layer.add(temp.right);
			if(start == end){
				end = layer.size();
				start = 0;
				result.add(layerList);
				layerList = new ArrayList<>();
			}
		}
		return result;
    }
	//递归
	ArrayList<ArrayList<Integer> > Print1(TreeNode pRoot) {
		ArrayList<ArrayList<Integer>> result = new ArrayList<>();
		depth(pRoot,1,result);
		return result;
	}
	private void depth(TreeNode pRoot,int depth,ArrayList<ArrayList<Integer>> list){
		if(pRoot == null)
			return;
		if(depth > list.size())
			list.add(new ArrayList<Integer>());
		list.get(depth-1).add(pRoot.val);
		depth(pRoot.left,depth+1,list);
		depth(pRoot.right,depth+1,list);
	}
	
	
	
}

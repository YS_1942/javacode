import java.util.LinkedList;

/**
 * 圆圈中最后剩下的数
 * */
public class NumRemainInCircle {
	public int LastRemaining_Solution(int n, int m) {
        if(n<1 || m<1)
        	return -1;
//        int last = 0;
//        for (int i = 2; i <= n ; i++) {
//			last = (last+m)%i;
//		}
//        return last;
        return solution(n, m);
    }
	public int solution(int n,int m){
		if(n == 1)
			return 0;
		else{
			int i = (solution(n-1, m)+m)%n;
//			System.out.println(i);
			return i;
		}
			
	}
	public int LastRemaining_Solution1(int n, int m) {
        LinkedList<Integer> list = new LinkedList<Integer>();
        for (int i = 0; i < n; i ++) {
            list.add(i);
        }
         
        int bt = 0;
        while (list.size() > 1) {
            bt = (bt + m - 1) % list.size();
            list.remove(bt);
        }
        return list.size() == 1 ? list.get(0) : -1;
    }
		
	public static void main(String[] args) {
		NumRemainInCircle n = new NumRemainInCircle();
		System.out.println(n.LastRemaining_Solution(6, 4));
	}
}

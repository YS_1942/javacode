import java.util.ArrayList;

/**
 * 给定一棵二叉搜索树，请找出其中的第k小的结点。
 * 例如， （5，3，7，2，4，6，8）    中，按结点数值大小顺序第三小结点的值为4。
 * */
public class Main63 {
	ArrayList<TreeNode> list = new ArrayList<>();
	TreeNode KthNode(TreeNode pRoot, int k){
		if(pRoot == null || k<1)
			return null;
		middleLoop(pRoot, list);
		if(k>list.size())
			return null;
		return list.get(k-1);
	}
	void middleLoop(TreeNode root,ArrayList<TreeNode> arr){
		if(root == null) return;
		middleLoop(root.left, arr);
		arr.add(root);
		middleLoop(root.right, arr);
	}
}

import java.util.*;

/**
 * 给定二维平面上的n个点，找出位于同一直线上的最大点数。
 * @author dell
 *
 */
class Point {
	int x;
    int y;
    Point() { x = 0; y = 0; }
    Point(int a, int b) { x = a; y = b; }
}
public class MaxPointsOnALine {
	public int maxPoints(Point[] points) {
        int length = points.length;
		if(length < 2) return length;
		int max = 0;
		for (int i = 0; i < length; i++) {
			Map<Float, Integer> map = new HashMap<>();
			int same = 0,straght = 0;
			Point a = points[i];
			for (int j = 0; j < points.length; j++) {
				if(i==j) continue;
				Point b = points[j];
				if(a.x == b.x){
					if(a.y == b.y)
						same++;
					else
						straght++;
				}
				else{
					float k = (float)(a.y - b.y)/(a.x -b.x);
					map.put(k, map.get(k)==null?1:map.get(k)+1);
				}
			}
			int tmp_max = straght;
			for (Float k : map.keySet()) {
				tmp_max = tmp_max > map.get(k)?tmp_max:map.get(k);
			}
			max = max > tmp_max+same+1?max:tmp_max+same+1;
		}
		return max;
    }
}

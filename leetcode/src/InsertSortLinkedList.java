/**
 * 使用插入排序对链表排序。
 * @author dell
 *
 */
public class InsertSortLinkedList {
	public ListNode insertionSortList(ListNode head) {
        ListNode dumy = new ListNode(0);
        ListNode cur = head;
        ListNode pre = dumy;
        while(cur != null){
        	ListNode next = cur.next;
        	pre = dumy;
        	while(pre.next != null &&pre.next.val<cur.val)
        		pre = pre.next;
        	cur.next = pre.next;
        	pre.next = cur;
        	cur = next;
        }
        return dumy.next;
    }
}

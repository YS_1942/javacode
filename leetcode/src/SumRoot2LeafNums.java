/**
给定一个只包含0到9位数字的二叉树，每个根到叶路径可以表示一个数字。
例如，根到叶路径1->2->3表示数字123。
找到所有根到叶数的总和。

*/
public class SumRoot2LeafNums {

	public int sumNumbers(TreeNode root) {
        return sumUp(root, 0);
    }
	public int sumUp(TreeNode node,int tmp){
		if(node == null)
        	return 0;
		tmp = tmp*10 + node.val;
		int left =0,right =0;
		if(node.left!=null)
			left = sumUp(node.left, tmp);
		if(node.right!=null)
			right = sumUp(node.right, tmp);
		if((left | right) == 0)
			return tmp;
		return left + right;
	}
}

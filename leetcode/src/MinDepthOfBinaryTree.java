import java.util.LinkedList;
import java.util.Queue;

/**
 * 给定二叉树，求其最小深度。最小深度是从根节点到最近的叶节点沿最短路径的节点数。
 * @author dell
 *
 */
class TreeNode {
	int val;
    TreeNode left;
    TreeNode right;
    TreeNode(int x) { val = x; }
}
public class MinDepthOfBinaryTree {
	public int run(TreeNode root) {
		if(root == null)
			return 0;
		if(root.left == null && root.right == null)
			return 1;
		int depth = 0;
		Queue<TreeNode> queue = new LinkedList<>();
		queue.offer(root);
		while(!queue.isEmpty()){
			int len = queue.size();
			depth++;
			for (int i = 0; i < len; i++) {
				TreeNode node = queue.poll();
				if(node.left == null && node.right == null)
					return depth;
				if(node.left != null)
					queue.offer(node.left);
				if(node.right != null)
					queue.offer(node.right);
			}
		}
		return 0;
    }
}

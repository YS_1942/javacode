import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Queue;

/**
输入二叉树，从底向上返回二叉树的层序遍历
输入：
    3
   / \
  9  20
    /  \
   15   7
输出：
[
  [15,7]
  [9,20],
  [3],
]
*/
public class LevelorderOfBT2 {
	public ArrayList<ArrayList<Integer>> levelOrderBottom(TreeNode root) {
		ArrayList<ArrayList<Integer>> res = new ArrayList<>();
		if(root == null)
			return res;
		Queue<TreeNode> queue = new LinkedList<>();
		queue.offer(root);
		while(!queue.isEmpty()){
			ArrayList<Integer> tmp = new ArrayList<>();
			int size = queue.size();
			//这里一定要用一个size取queue的size()，因为queue的长度一直在变！！！
			for (int i = 0; i < size; i++) {
				TreeNode node = queue.poll();
				if(node != null){
					if(node.left != null)
						queue.offer(node.left);
					if(node.right != null)
						queue.offer(node.right);
					tmp.add(node.val);
				}
			}
			res.add(tmp);
		}
		//反序操作，或者res.add(0,tmp)
		res.sort(new Comparator<ArrayList<Integer>>() {
			@Override
			public int compare(ArrayList<Integer> o1, ArrayList<Integer> o2) {
				// TODO Auto-generated method stub
				return -1;
			}
		});
		return res;
    }
	//调用递归，每次先递归下一层，再插入本层
	ArrayList<ArrayList<Integer>> res = new ArrayList<>();
	public ArrayList<ArrayList<Integer>> levelOrderBottom2(TreeNode root) {
		if(root == null) return res;
		Queue<TreeNode> queue = new LinkedList<>();
		queue.offer(root);
		help(queue);
		return res;
	}
	public void help(Queue<TreeNode> queue){
		if(queue.isEmpty()){
			return;
		}
		int size = queue.size();
		ArrayList<Integer> tmp = new ArrayList<>();
		for (int i = 0; i < size; i++) {
			TreeNode node = queue.poll();
			if(node.left != null)
				queue.offer(node.left);
			if(node.right != null)
				queue.offer(node.right);
			tmp.add(node.val);
		}
		help(queue);
		res.add(tmp);
	}
}

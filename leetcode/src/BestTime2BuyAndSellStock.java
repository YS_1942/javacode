/**
假设您有一个数组，其中第i个元素是第i天给定股票的价格。
只能完成最多一笔交易（即，买一份股票，卖一份股票），设计一个算法来找到最大利润。
*/
public class BestTime2BuyAndSellStock {
	public int maxProfit(int[] prices) {
		//找极小值，和它后面的极大值，找差值的最大者
        if(prices==null||prices.length==0){
            return 0;
        }
        int max=0;
        int min=prices[0];
        for(int i=0;i<prices.length;i++){
            min = Math.min(min,prices[i]);
            max=Math.max(max,prices[i]-min);
        }
        return max;
    }
	public static void main(String[] args) {
		BestTime2BuyAndSellStock b = new BestTime2BuyAndSellStock();
		int[] p = new int[]{4,3,2,4,7};
		System.out.println(b.maxProfit(p));
	}
}

/**
 * 

输入：
         1
       /  \
      2    3
     / \  / \
    4  5  6  7
输出：
         1 -> NULL
       /  \
      2 -> 3 -> NULL
     / \  / \
    4->5->6->7 -> NULL

完美二叉树，每个叶子都在同一级别，每个父结点都有两个孩子
空间复杂度O(n)
*
*/
class  TreeLinkNode {
    TreeLinkNode left;
    TreeLinkNode right;
    TreeLinkNode next;
  }
public class PopulateNextRightNode {
	public void connect(TreeLinkNode root) {
        if(root== null)
        	return;
        help(root);
    }
	public void help(TreeLinkNode node){
		if(node.left == null || node.right == null)
			return ;
		node.left.next = node.right;
		if(node.next != null)
			node.right.next = node.next.left;
		help(node.left);
		help(node.right);
	}
}

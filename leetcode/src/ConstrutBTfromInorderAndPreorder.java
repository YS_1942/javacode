/**
给定树的中序遍历和后序遍历，构造二叉树。
假定不存在重复
*/
public class ConstrutBTfromInorderAndPreorder {
	public TreeNode buildTree(int[] preorder, int[] inorder) {
		if(inorder == null || inorder.length == 0)
        	return null;
        return help(inorder, 0, inorder.length-1, preorder, 0, preorder.length-1);
    }
	public TreeNode help(int[] inorder,int inStart,int inEnd,int[] preorder,int preStart,int preEnd){
		if(inStart > inEnd || preStart > preEnd) return null;
		TreeNode root = null;
		for (int i = inStart; i <= inEnd ; i++) {
			if(inorder[i] == preorder[preStart]){
				root = new TreeNode(inorder[i]);
				root.left = help(inorder, inStart, i-1, preorder, preStart+1, preStart+i-inStart);
				root.right = help(inorder, i+1, inEnd, preorder, preStart+i-inStart+1, preEnd);
				break;
			}
		}
		return root;
	}
}

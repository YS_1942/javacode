/**
 * 给定一个链表，确定它是否有一个循环。
 * 跟进：你能在不使用额外空间的情况下解决它吗？
 * @author dell
 *
 */
public class CycleInLinkedList {
	public boolean hasCycle(ListNode head) {
		ListNode slow = head;
		ListNode fast = head;
		while(fast!=null && fast.next !=null){
			fast = fast.next.next;
			slow = slow.next;
			if(fast == slow)
				return true;
		}
		return false;
    }
}

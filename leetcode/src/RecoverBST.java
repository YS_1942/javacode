/**
二叉搜索树（BST）的两个元素被错误地交换。
恢复树而不改变其结构。

空间复杂度O(1)


*/
//节点交换分两种情况，一种是相邻节点，这种交换不破坏两节点与之外的大小性，只破坏
//两节点之间大小性。另一种是不相邻，这种破坏两节点分别与两边的节点。
//因此在第一次搜到一个不满足的节点后，要将第二个节点也赋指针，以考虑第一种情况
public class RecoverBST {
	TreeNode pre,p1,p2;
	public void recoverTree(TreeNode root) {
        inTraversal(root);
        int tmp = p1.val;
        p1.val = p2.val;
        p2.val = tmp;
    }
	private void inTraversal(TreeNode root){
		if(root == null)
			return ;
		inTraversal(root.left);
		if(pre!=null&&pre.val > root.val){
			if(p1 == null){
				p1 = pre;
				p2 = root;
			}
			else
				p2 = root;
		}
		pre = root;
		inTraversal(root.right);
	}
}

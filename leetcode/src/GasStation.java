/**
 沿环形路线有n个加油站，其中一个加油站i的天然气量为gas[i]。
你有一辆没有限制油箱的车，从第一站i到下一站i+1需要cost[i]的汽油。你从一个加油站的空油箱开始旅行。
如果你能绕着电路走一圈，返回启动加油站的索引，否则返回-1。
 * */
public class GasStation {
	public int canCompleteCircuit(int[] gas, int[] cost) {
		int n = gas.length;
        for (int i = 0; i < n; i++) {
			int res = 0;
			int start = i;
			int j;
			boolean flag = false;
			for(j = 0;j<n;j++){
				res += gas[start];
				if(res >= cost[start]){
					res -= cost[start];
					start=(start+1)%n;
					flag = true;
				}
				else{
					flag = false;
					break;
				}
			}
			if(flag)
				return i;
		}
        return -1;
    }
	//贪心算法
	public int canCompleteCircuit1(int[] gas, int[] cost) {
		int start = -1;
		int total = 0,sum=0;
		for (int i = 0; i < gas.length; i++) {
			sum += gas[i] - cost[i];
			total += gas[i] - cost[i];
			if(sum < 0){
				sum = 0;
				start = i;
			}
		}
		return total >= 0? start +1 :-1;
	}
	public int canCompleteCircuit2(int[] gas, int[] cost) {
		int start = gas.length - 1;
		int end = 0;
		int sum = gas[start] - cost[start];
		while(start > end){
			if(sum >= 0){
				sum += gas[end] - cost[end];
				end++;
			}
			else{
				start--;
				sum += gas[start] - cost[start];
			}
		}
		return sum>=0?start:-1;
	}
}

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * 给定一个字符串s和单词字典dict，在s中添加空格来构造一个句子，
 * 其中每个单词都是有效的字典单词。
 * 返回所有可能的句子。
 * s ="catsanddog",
 * dict =["cat", "cats", "and", "sand", "dog"].
 * 返回["cats and dog", "cat sand dog"].
 * @author dell
 *
 */
public class StringInDict2 {
	 public ArrayList<String> wordBreak(String s, Set<String> dict) {
		 return DFS(s, dict, new HashMap<String,ArrayList<String>>());
	 }
	 public ArrayList<String> DFS(String s, Set<String> dict, HashMap<String, ArrayList<String>> map){
		 if(map.containsKey(s))
			 return map.get(s);
		 ArrayList<String> res = new ArrayList<>();
		 if(s.length() == 0){
			 res.add("");
			 return res;
		 }
		 for (String str : dict) {
			if(s.startsWith(str)){
				for (String string : DFS(s.substring(str.length()), dict, map)) {
					res.add(str+(string == ""?"":" ")+string);
				}
			}
		}
		 map.put(s, res);
		 return res;
	 }
}

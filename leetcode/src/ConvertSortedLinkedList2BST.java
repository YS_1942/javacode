/**
给定一个单链表，其中元素按升序排序，将其转换为平衡二叉搜索树。
和数组转换为BST思路相同，数组可以直接访问下标，链表需要指针
*/
public class ConvertSortedLinkedList2BST {
	public TreeNode sortedListToBST(ListNode head) {
        return toBST(head, null);
    }
	private TreeNode toBST(ListNode head, ListNode tail){
		if(head == tail)
			return null;
		//两个指针，一快一慢，找中间结点作为root
		ListNode fast = head;
		ListNode slow = head;
		while(fast != tail && fast.next != tail){
			fast = fast.next.next;
			slow = slow.next;
		}
		TreeNode root = new TreeNode(slow.val);
		root.left = toBST(head, slow);
		root.right = toBST(slow.next, tail);
		return root;
	}
}

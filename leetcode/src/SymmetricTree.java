/**
 *对于二叉树，检查它是否是自身的镜像（即，围绕其中心对称）。
 * */
public class SymmetricTree {
	public boolean isSymmetric(TreeNode root) {
		if(root == null)
			return true;
		return help(root.left, root.right);
	}
	public boolean help(TreeNode left,TreeNode right) {
		if(left == null && right == null)
			return true;
		if(left == null || right == null)
			return false;
		if(left.val != right.val){
			return false;
		}
		return help(left.left, right.right) && help(left.right, right.left);
	}
}

/**
给定s1，s2，s3，判断s3是否由s1和s2交织而成。
给定：
S1=“aabcc”，
S2=“dbbca”，
当s3=“aadbbcbcac”时，返回true。
当s3=“aadbbbaccc”时，返回false。

*/
public class InterleavingString {
	public boolean isInterleave(String s1, String s2, String s3) {
        return help(s1, 0, s2, 0, s3, 0);
    }
	public boolean help(String s1,int index1,String s2,int index2,String s3,int index3){
		if(index1 == s1.length()&&index2 ==s2.length()&&index3 == s3.length())
			return true;
		if(index3 == s3.length())
			return false;
		if(index1 == s1.length()){
			if(s2.substring(index2).equals(s3.substring(index3)))
				return true;
			else{
				return false;
			}
		}
		if(index2 == s2.length()){
			if(s1.substring(index1).equals(s3.substring(index3)))
				return true;
			else{
				return false;
			}
				
		}
		if(s1.charAt(index1)==s3.charAt(index3) && s2.charAt(index2)==s3.charAt(index3)){
			return help(s1, index1+1, s2, index2, s3, index3+1) || help(s1, index1, s2, index2+1, s3, index3+1);
		}
		if(s1.charAt(index1)==s3.charAt(index3)){
			return help(s1, index1+1, s2, index2, s3, index3+1);
		}
		if(s2.charAt(index2)==s3.charAt(index3)){
			return help(s1, index1, s2, index2+1, s3, index3+1);
		}
		return false;
	}
	//动态规划
	public boolean isInterleave2(String s1, String s2, String s3) {
		int len1 = s1.length();
		int len2 = s2.length();
		int len3 = s3.length();
		if(len1 + len2 != len3) return false;
		char[] ch1 = s1.toCharArray();
		char[] ch2 = s2.toCharArray();
		char[] ch3 = s3.toCharArray();
		
		boolean[][] dp = new boolean[len1+1][len2+1];
		dp[0][0] = true;
		//第一列
		for (int i = 1; i < len2 + 1; i++) {
			dp[0][i] = dp[0][i-1] && ch2[i-1] == ch3[i-1];
		}
		//第一行
		for (int i = 1; i < len1 + 1; i++) {
			dp[i][0] = dp[i-1][0] && ch1[i-1] == ch3[i-1];
		}
		for (int i = 1; i < len1 + 1; i++) {
			for (int j = 1; j < len2 + 1; j++) {
				dp[i][j] = (dp[i-1][j] && (ch1[i-1] == ch3[i+j-1]))
						|| (dp[i][j-1] &&(ch2[j-1] == ch3[i+j-1]));
			}
		}
		return dp[len1][len2];
	}
}


public class PartitionList {
	public ListNode partition(ListNode head, int x) {
        ListNode p1 = new ListNode(0);
        ListNode p2 = new ListNode(0);
        ListNode pp1 = p1;
        ListNode pp2 = p2;
        while(head != null){
        	if(head.val<x){
        		pp1.next = head;
        		pp1 = pp1.next;
        	}
        	else{
        		pp2.next = head;
        		pp2 = pp2.next;
        	}
        	head = head.next;
        }
        pp1.next = p2.next;
        pp2.next = null;
        return p1.next;
    }
}

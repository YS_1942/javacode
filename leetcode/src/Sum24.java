import java.util.*;

/**24点：给定n个1~23的整数，每个数最多用一次，求能凑出24点的组合数*/
public class Sum24 {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		String str = s.nextLine();
		String[] strs = str.split(" ");
		int[] a = new int[strs.length];
		int[] f = new int[25];
		f[0] = 1;
		for (int i = 0; i < a.length; i++) {
			a[i] = Integer.parseInt(strs[i]);
		}
		for (int i = 0; i < a.length; i++) {
			for (int j = 24; j >= a[i] ; j--) {
				f[j] += f[j-a[i]];
			}
		}
		System.out.println(f[24]);
	}
}

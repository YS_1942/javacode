

/**
 有N个孩子排成一行。为每个孩子分配一个分级值。
你给这些孩子糖果要符合以下要求：
每个孩子必须至少有一颗糖果。
评分高的孩子比他们的邻居得到更多的糖果。
你必须给多少糖果？
 * */
public class Candy {
	public int candy(int[] ratings) {
        int[] count = new int[ratings.length];
//        Arrays.fill(count, 1);
        int result = 0;
        for (int i = 1; i < ratings.length; i++) {
			if(ratings[i] > ratings[i-1])
				count[i] = count[i-1] + 1;
		}
        for (int i = ratings.length-1; i > 0; i--) {
        	//第二个条件很重要
        	result += count[i];
			if(ratings[i] < ratings[i-1] && count[i] >= count[i-1])
				count[i-1] = count[i] + 1; 
		}
        result += count[0];
        return result + ratings.length;
	}
}

/**
给定一个二进制树，确定它是否是有效的二叉搜索树（BST）。
假设BST定义如下：
节点的左子树只包含键小于节点键的节点。
节点的右子树只包含键大于节点键的节点。
左子树和右子树也必须是二叉搜索树。
*/
public class ValidBST {
	//中序遍历
	TreeNode pre;
	public boolean isValidBST(TreeNode root) {
        pre = null;
        return inTraversal(root);
    }
	public boolean inTraversal(TreeNode root){
		if(root == null)
			return true;
		if(inTraversal(root.left)){
			if(pre!=null && pre.val <= root.val)
				return false;
			pre = root;
			return inTraversal(root.right);
		}
		return false;
	}
	//记录上下限
	public boolean isValidBST2(TreeNode root) {
		return help(root, Integer.MIN_VALUE, Integer.MAX_VALUE);
	}
	public boolean help(TreeNode root,int low, int high){
		if(root == null) return true;
		if(root.val<=low ||root.val>=high) return false;
		return help(root.left, low, root.val) && help(root.right, root.val, high);
	}
}

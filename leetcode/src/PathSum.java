/**
给定一个二叉树和一个和，确定该树是否有一个根到叶的路径，
以便将路径上的所有值相加等于给定的和。
*
*/
public class PathSum {
	public boolean hasPathSum(TreeNode root, int sum) {
        if(root == null)
        	return false;
        if(root.left == null && root.right == null && sum - root.val == 0)
        	return true;
        return hasPathSum(root.left, sum-root.val)||hasPathSum(root.right, sum-root.val);
    }
}

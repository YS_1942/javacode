import java.util.Scanner;

/**G社正在开发一个新的战棋类游戏，在这个游戏中，
 * 角色只能向2个方向移动：右、下。移动需要消耗行动力，
 * 游戏地图上划分M*N个格子，当角色移动到某个格子上时，
 * 行动力就会加上格子上的值K（-100~100），
 * 当行动力小于等于0时游戏失败，
 * 请问要从地图左上角移动到地图右下角至少需要多少起始行动力，
 * 注意（玩家初始化到起始的左上角格子时也需要消耗行动力）。
*/
public class WalkGrid {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int m = s.nextInt();
		int n = s.nextInt();
		int[][] vals = new int[m][n];
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				vals[i][j] = s.nextInt();
			}
		}
		int[][] dp = new int[m+1][n+1];
		for (int i = m-1; i >= 0; i--) {
			for (int j = n-1; j >= 0; j--) {
				dp[i][j] = Math.min(Math.max(1, dp[i+1][j]), Math.max(1, dp[i][j+1]))-vals[i][j];
			}
		}
		dp[0][0] = dp[0][0]>0?dp[0][0]:1;
		System.out.println(dp[0][0]);
	}
}

import java.util.ArrayList;
/**
 * 给定一个字符串s，分区s使得分区的每个子串都是回文。
返回s的所有可能的回文分区。
 s ="aab"。返回
 [
    ["aa","b"],
    ["a","a","b"]
  ]
*/
public class PalindromePartitioning {
	//回朔法
	public ArrayList<ArrayList<String>> partition(String s) {
		ArrayList<ArrayList<String>> result = new ArrayList<>();
		findPalindrome(result, new ArrayList<>(),s);
		return result;
    }
	public void findPalindrome(ArrayList<ArrayList<String>> result,ArrayList<String>temp,String s){
		if(s.length() == 0){
			result.add(new ArrayList<>(temp));
			return;
		}
		for (int i = 1; i <= s.length(); i++) {
			String t = s.substring(0, i);
			if(isPalindrome(t)){
				temp.add(t);
				findPalindrome(result, temp, s.substring(i));
				temp.remove(temp.size()-1);
			}
		}
	}
	public boolean isPalindrome(String str){
		return new StringBuilder(str).reverse().toString().equals(str);
//		if(str.length()==1)
//			return true;
//		int start = 0;
//		int end = str.length()-1;
//		while(start < end){
//			if(str.charAt(start)==str.charAt(end)){
//				start++;
//				end--;
//			}
//			return false;
//		}
//		return true;
	}
}

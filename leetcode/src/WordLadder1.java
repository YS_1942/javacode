import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

/**
 * 给定两个词（开始和结束）和一个字典，找出从开始到结束的最短转换序列的长度，满足：
一次只能更改一个字母
词典中必须存在每个中间词
如：
start ="hit"
end ="cog"
dict =["hot","dot","dog","lot","log"]
最短转换："hit" -> "hot" -> "dot" -> "dog" -> "cog"
所以返回5
如果没有这样的转换序列，则返回0。
所有单词的长度都相同。
所有单词只包含小写字母字符。
 * */
public class WordLadder1 {
	public int ladderLength(String start, String end, HashSet<String> dict) {
		LinkedList<String> queue = new LinkedList<>();
        queue.offer(start);
        int res = 1;
        while(!queue.isEmpty()){
        	int size = queue.size();
        	for (int i = 0; i < size; i++) {
				String s = queue.poll();
        		if(differOnce(s, end)) 
        			return res+1;
        		for (Iterator<String> it = dict.iterator(); it.hasNext(); ) {
					String str = it.next();
					if(differOnce(str, s)){
						queue.offer(str);
						it.remove();
					}
				}
			}
        	res++;
        }
        return 0;
    }
	public boolean differOnce(String s1,String s2){
		int count = 0;
		for (int i = 0; i < s1.length(); i++) {
			if(s1.charAt(i) != s2.charAt(i))
				count++;
		}
		return count == 1?true:false;
	}
}

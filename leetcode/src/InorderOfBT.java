import java.util.ArrayList;
import java.util.Stack;

/**
给定二叉树，返回其节点值的中序遍历。
输入：
   1
    \
     2
    /
   3
输出：
[1,3,2]
*/
public class InorderOfBT {
	//递归
	public ArrayList<Integer> inorderTraversal(TreeNode root) {
		ArrayList<Integer> res = new ArrayList<>();
        if(root == null)
        	return res;
        help(root,res);
        return res;
    }
	public void help(TreeNode node,ArrayList<Integer> res){
		if(node == null)
			return;
		help(node.left,res);
		res.add(node.val);
		help(node.right,res);
	}
	//迭代 （非递归）
	public ArrayList<Integer> inorderTraversal1(TreeNode root) {
		ArrayList<Integer> res = new ArrayList<>();
		Stack<TreeNode> stack = new Stack<>();
		while(root != null || !stack.isEmpty()){
			while(root != null){
				stack.push(root);
				root = root.left;
			}
			root = stack.pop();
			res.add(root.val);
			root = root.right;
		}
		return res;
	}
}

import java.util.LinkedList;
import java.util.Queue;

/**
跟进前一个问题。
如果给定的树可以是任何二叉树呢？您以前的解决方案是否仍然有效？
输入：
         1
       /  \
      2    3
     / \    \
    4   5    7
输出：
         1 -> NULL
       /  \
      2 -> 3 -> NULL
     / \    \
    4-> 5 -> 7 -> NULL    

*/
public class PopulateNextRightNode2 {
	public void connect(TreeLinkNode root) {
        if(root == null)
        	return;
        Queue<TreeLinkNode> queue = new LinkedList<>();
        queue.offer(root);
        TreeLinkNode tmp = null;
        while(!queue.isEmpty()){
        	int len = queue.size();
        	for (int i = 0; i < len; i++) {
				tmp = queue.poll();
				if(i == len -1)
					tmp.next = null;
				else
					tmp.next = queue.peek();
				if(tmp.left != null)
					queue.offer(tmp.left);
				if(tmp.right != null)
					queue.offer(tmp.right);
        	}
        }
	}
}

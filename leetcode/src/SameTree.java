/**
给定两个二叉树，编写一个函数来检查它们是否相等。
如果两个二叉树结构相同且节点具有相同的值，则认为它们相等。
*/
public class SameTree {
	public boolean isSameTree(TreeNode p, TreeNode q) {
		if(p == null && q == null)
			return true;
		if(p == null || q == null)
			return false;
		if(p.val != q.val){
			return false;
		}
		return isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
    }
}

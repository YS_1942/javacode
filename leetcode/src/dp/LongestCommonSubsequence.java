package dp;
/**
最长公共自序列问题：
给定两个序列 
X={x1,x2,...,xm}
Y={y1,y2,...,yn}
求X和Y长度最长的公共子序列
*/
public class LongestCommonSubsequence {
	public char[][] lcs_Down2Up(char[] x,char[] y,int[][] c){
		int m = x.length;
		int n = y.length;
		char[][] b = new char[m][n];
		for (int i = 1;i <= m; i++) {
			for (int j = 1; j <= n; j++) {
				if(x[i-1] == y[j-1]){
					c[i][j] = c[i-1][j-1] + 1;
					b[i-1][j-1] = '7';
				}
				else if(c[i-1][j] >= c[i][j-1]){
					c[i][j] = c[i-1][j];
					b[i-1][j-1] = '8';
				}
				else{
					c[i][j] = c[i][j-1];
					b[i-1][j-1] = '4';
				}
			}
		}
		return b;
	}
	public static void main(String[] args) {
		char[] x = new char[]{'1','2','3','2','4','1','2'};
		char[] y = new char[]{'2','4','3','1','2','1'};
		int[][]c = new int[x.length+1][y.length+1];
		new LongestCommonSubsequence().lcs_Down2Up(x, y, c);
		System.out.println(c[x.length][y.length]);
	}
}

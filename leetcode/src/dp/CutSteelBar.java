package dp;

import java.util.Arrays;

/**
给定一段长度为n的钢条，和一个价格表p，求切割方案，使得收益r最大
长度i  0 1 2 3 4 5  6  7  8  9  10
价格pi[0 1 5 8 9 10 17 17 20 24 30]
输入n = 4,
输出r = 10;
 */
public class CutSteelBar {
	/**
	带备忘的自顶向下方法
	 * */
	//改进的递归，用空间换时间，将子问题的解纪录下来，避免多次递归子问题
	public int cutUp2Down(int n,int[] p,int[] s){
		//记录收益
		int[] r = new int[n+1];
		//初始化结果数组
		Arrays.fill(r, -1);
		return cut_help(n, p, r, s);
	}
	public int cut_help(int n,int[] p,int[] r,int[] s){
		//检查结果是否已知,若已知则不需要递归，直接返回
		if(r[n] >= 0)
			return r[n];
		int q;
		//计算当前的最优值
		if(n == 0)
			q = 0;
		else{
			q = -1;
			//将r[]数组从前往后填充
			for (int i = 1; i <= n; i++) {
				if(q < p[i]+cut_help(n-i, p, r,s)){
					q = p[i]+cut_help(n-i, p, r,s);
					s[n] = i;
				}
				//若不需要切割方案，可直接用下面这句
//				q = Math.max(q, p[i]+cut_help(n-i, p, r,s));
			}
		}
		r[n] = q;
		return q;
	}
	/**
	自底向上，
	 * */
	public int cutDown2Up(int n,int[] p,int[] s){
		int[] r = new int[n+1];
		//两层循环
		for (int i = 1; i <= n; i++) {
			int q = -1;
			for (int j = 1; j <= i ; j++) {
				if(q < p[j] + r[i-j]){
					q = p[j] + r[i-j];
					s[i] = j;
				}
			}
			r[i] = q;
		}
		return r[n];
	}
	public static void main(String[] args) {
		int[] p = new int[]{0,1,5,8,9,10,17,17,20,24,30};
		//记录切割方案
		int[] s = new int[11];
		CutSteelBar csb = new CutSteelBar();
		for (int i = 1; i < 11; i++) {
			int m = csb.cutDown2Up(i, p, s);
			System.out.print("r"+i+"="+m+"\t");
			int n = i;
			while(n>0){
				System.out.print(s[n]+" ");
				n = n-s[n];
			}
			System.out.println();
		}
	}
}

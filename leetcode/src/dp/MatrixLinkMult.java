package dp;
/**
矩阵链乘问题，求解计算乘法次数最少的方案。
给定n个矩阵的链，矩阵Ai的规模为pi*pi+1,求完全括号方案，使计算乘积A1A2…An所需标量乘法次数最少
 * */
public class MatrixLinkMult {
	/**自底向上*/
	public int multMatrix_Down2Up(int[] p,int n,int[][] m,int[][] s){
		//先按矩阵链长度遍历
		for (int l = 2; l <= n; l++) {
			//起点矩阵
			for (int i = 0; i <= n-l; i++) {
				//矩阵链的最后一个矩阵
				int j = i+l-1;
				//需要用二维矩阵保存最优解（子问题与位置有关）
				m[i][j] = Integer.MAX_VALUE;
				//遍历所有的切割点，将矩阵链一分为二递归子问题
				for (int k = i; k <= j-1; k++) {
					//计算每个切割点的计算次数
					int q = m[i][k]+m[k+1][j]+p[i]*p[k+1]*p[j+1];
					if(q < m[i][j]){
						m[i][j] = q;
						s[i][j] = k;
					}
				}
			}
		}
		return m[0][n-1];
	}
	/**带备忘录的自顶向下*/
	public int multMatrix_Up2Down(int[] p,int[][] m,int[][] s,int i,int j){
		if(i == j)
			return 0;
		if(m[i][j] != 0)
			return m[i][j];
		m[i][j] = Integer.MAX_VALUE;
		for (int k = i; k < j; k++) {
			int q = multMatrix_Up2Down(p, m,s, i, k)+multMatrix_Up2Down(p, m,s, k+1, j)+p[i]*p[k+1]*p[j+1];
			if(q < m[i][j]){
				m[i][j] = q;
				s[i][j] = k;
			}
		}
		return m[i][j];
	}
		
	//打印最优解
	public void printMatrix(int[][]s,int i,int j){
		if(i == j)
			System.out.print("A"+(i+1));
		else{
			System.out.print("(");
			printMatrix(s, i, s[i][j]);
			printMatrix(s, s[i][j]+1, j);
			System.out.print(")");
		}
	}
	public static void main(String[] args) {
		MatrixLinkMult dp = new MatrixLinkMult();
		//矩阵连乘验证
		int[] p = new int[]{30,35,15,5,10,20,25};
		int n = p.length-1;
		//可以传入一个矩阵m保存每个最优解，也可以不传入，只返回最后一次结果
		int[][] m = new int[n][n];
		int[][] s = new int[n][n];
		System.out.println(dp.multMatrix_Up2Down(p,m,s,0,n-1));
		dp.printMatrix(s, 0, n-1);		
	}
}

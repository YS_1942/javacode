package dp;
/**
通过只删除（但不重新排序）s中的字符，可以用多少种方法生成字符串t？
输入： s = "rabbbit",t = "rabbit"
输出： 3
*/
public class Distinct_Subsequences {
	public int numDistinct(String S, String T) {
        if(S == null || T == null || T.length() == 0|| S.length() < T.length())
        	return 0;
        int sLen = S.length();
        int tLen = T.length();
        //与矩阵链乘一样，需要二维数组保存结果
        int[][] array = new int[sLen+1][tLen+1];
        //初始化第一行，S=""时,结果为0（java自带初始化为0，可以不需要）
        //初始化第一列，T=""时,结果为1（每个字符串都包含空子串）
        for (int i = 0; i < sLen+1; i++) {
			array[i][0] = 1;
		}
        for (int i = 1; i < sLen+1; i++) {
        	//子序列不能长于原序列
			for (int j = Math.min(i, tLen); j > 0; j--) {
				if(S.charAt(i-1) != T.charAt(j-1))
					array[i][j] = array[i-1][j];
				else
					array[i][j] = array[i-1][j]+array[i-1][j-1];
			}
		}
        return array[sLen][tLen];
    }
	//可以降维到一维数组
	public int numDistinct2(String S, String T) {
		if(S == null || T == null || T.length() == 0|| S.length() < T.length())
        	return 0;
        int sLen = S.length();
        int tLen = T.length();
        int[] array = new int[tLen+1];
        array[0] = 1;
        for (int i = 1; i < sLen + 1; i++) {
        	//必须从后往前
			for (int j = Math.min(i, tLen); j > 0; j--) {
				if(S.charAt(i-1) == T.charAt(j-1))
					array[j] = array[j] + array[j-1];
			}
		}
        return array[tLen];
	}
	
	
}

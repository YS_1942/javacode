import java.util.*;
/**从非负整数序列 0, 1, 2, ..., n 中给出包含其中 n 个数的子序列，请找出未出现在该子序 列中的那个数。
输入
3 3 0 1
输出
2
 * 
 * */
public class FindNumInSequence {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int n = s.nextInt();
		int res = 0;
		for (int i = 0; i < n; i++) {
			res ^= i;
			res ^= s.nextInt();
		}
		System.out.println(res^n);
	}
}

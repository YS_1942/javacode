import java.util.ArrayList;
import java.util.Arrays;

/**
给定索引k，返回第k行的杨辉三角。
如输入 k = 3,
输出
[1,3,3,1]

空间复杂度 O(k)
 * */
public class PascalsTriangle2 {
	public ArrayList<Integer> getRow(int rowIndex) {
		//行数差一行
		ArrayList<Integer> res = new ArrayList<>();
        for (int i = 0; i <= rowIndex; i++) {
        	res.add(1);
			for (int j = i; j >=0; j--) {
				if(j == 0 || j == i)
					continue;
				else
					res.set(j, res.get(j-1)+res.get(j));
			}
		}
        return res;
    }
	public static void main(String[] args) {
		PascalsTriangle2 p = new PascalsTriangle2();
		System.out.println(Arrays.toString(p.getRow(3).toArray()));
	}
}

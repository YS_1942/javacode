/**
给定树的中序遍历和后序遍历，构造二叉树。
假定不存在重复
*/
public class ConstrutBTfromInorderAndPostorder {
	public TreeNode buildTree(int[] inorder, int[] postorder) {
        if(inorder == null || inorder.length == 0)
        	return null;
        return help(inorder, 0, inorder.length-1, postorder, 0, postorder.length-1);
	}
	public TreeNode help(int[] inorder,int inStart,int inEnd,int[] postorder,int postStart,int postEnd){
		if(inStart > inEnd || postStart > postEnd) return null;
		TreeNode root = null;
		for (int i = inStart; i <= inEnd ; i++) {
			if(inorder[i] == postorder[postEnd]){
				root = new TreeNode(inorder[i]);
				root.left = help(inorder, inStart, i-1, postorder, postStart, postStart+i-inStart-1);
				root.right = help(inorder, i+1, inEnd, postorder, postStart+i-inStart, postEnd-1);
				break;
			}
		}
		return root;
	}
}

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

class UndirectedGraphNode {
    int label;
    ArrayList<UndirectedGraphNode> neighbors;
    UndirectedGraphNode(int x) { label = x; neighbors = new ArrayList<UndirectedGraphNode>(); }
};
/**
 * 
 * */
public class CloneGraph {
	//DFS
	public UndirectedGraphNode cloneGraph(UndirectedGraphNode node) {
        Map<UndirectedGraphNode, UndirectedGraphNode> map = new HashMap<>();
        return dfs(node, map);
    }
	public UndirectedGraphNode dfs(UndirectedGraphNode node,Map<UndirectedGraphNode, UndirectedGraphNode> map){
		if(node == null)
			return null;
		UndirectedGraphNode tmp = new UndirectedGraphNode(node.label);
		map.put(node, tmp);
		for (UndirectedGraphNode neighboor : node.neighbors) {
			if(map.containsKey(neighboor)){
				tmp.neighbors.add(map.get(neighboor));
			}
			else{
				UndirectedGraphNode temp = dfs(neighboor, map);
				tmp.neighbors.add(temp);
			}
		}
		return tmp;
	}
	//BFS
	public UndirectedGraphNode cloneGraph1(UndirectedGraphNode node) {
		if(node == null)
			return null;
		LinkedList<UndirectedGraphNode> queue = new LinkedList<>();
		queue.push(node);
		Map<UndirectedGraphNode, UndirectedGraphNode> map = new HashMap<>();
		UndirectedGraphNode tmp = new UndirectedGraphNode(node.label);
		map.put(node, tmp);
		while(!queue.isEmpty()){
			UndirectedGraphNode temp = queue.pop();
			UndirectedGraphNode tempClone = map.get(temp);
			for (UndirectedGraphNode neighbor : temp.neighbors) {
				if(map.containsKey(neighbor))
					tempClone.neighbors.add(map.get(neighbor));
				else{
					UndirectedGraphNode s1 = new UndirectedGraphNode(neighbor.label);
					map.put(neighbor, s1);
					tempClone.neighbors.add(s1);
					queue.push(neighbor);
				}
			}
		}
		return tmp;
	}
}

import java.util.ArrayList;

/**
 * 杨辉三角
给定行数numrows，生成杨辉三角的前numrows行。
如，输r入numRows = 5,
输出：
[
     [1],
    [1,1],
   [1,2,1],
  [1,3,3,1],
 [1,4,6,4,1]
]
 * */
public class PascalsTriangle {
	public ArrayList<ArrayList<Integer>> generate(int numRows) {
		ArrayList<ArrayList<Integer>> res = new ArrayList<>(); 
		for (int i = 0; i < numRows; i++) {
			ArrayList<Integer> tmp = new ArrayList<>();
			for (int j = 0; j <= i ; j++) {
				if(j==0 || j==i)
					tmp.add(1);
				else
					tmp.add(res.get(i-1).get(j-1)+res.get(i-1).get(j));
			}
			res.add(tmp);
		}
		return res;
    }
}

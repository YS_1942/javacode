import java.util.HashSet;
import java.util.Set;

/**给定一个未排序的整数数组，求最长连续元素序列的长度。
输入:[100,4,200,1,3,2]
输出:4
 * 
 * 要求时间复杂度O(N)*/
public class LongestConsecutiveSequence {
	public int longestConsecutive(int[] num) {
        Set<Integer> set = new HashSet<>();
        for (int n : num) {
			set.add(n);
		}
        int max = 1;
        for (int n : num) {
			if(set.remove(n)){
				int val = n;
				int sum = 1;
				int val_small = val-1;
				int val_big = val+1;
				while(set.remove(val_small)){
					sum++;
					val_small--;
				}
				while(set.remove(val_big)){
					sum++;
					val_big++;
				}
				max = Math.max(sum, max);
			}
		}
		return max;
    }
}

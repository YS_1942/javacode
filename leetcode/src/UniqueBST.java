import java.util.ArrayList;

/**
给定n，有多少结构上唯一的BST（二进制搜索树）存储值1…n？
例如，
如果n=3，则总共有5个唯一的BST。
   1         3     3      2      1
    \       /     /      / \      \
     3     2     1      1   3      2
    /     /       \                 \
   2     1         2                 3
*/
//思路：左子树个数和右子树个数乘积累加
//dp[i] = sigma(dp[k]dp[i-k])
public class UniqueBST {
	public int numTrees(int n) {
        if(n < 0) return -1;
        int[] dp = new int[n+1];
        dp[0] = 1;
        dp[1] = 1;
        for (int i = 2; i < n+1; i++) {
			for (int j = 0; j < i ; j++) {
				dp[i] += dp[j]*dp[i-j-1];
			}
		}
        return dp[n];
    }
	//生成这些BST
	public ArrayList<TreeNode> generateTrees(int n) {
        return createTree(1, n);
    }
	public ArrayList<TreeNode> createTree(int low,int high){
		ArrayList<TreeNode> res = new ArrayList<>();
		if(low > high){
			res.add(null);
			return res;
		}
		for (int i = low; i <= high; i++) {
			ArrayList<TreeNode> left = createTree(low, i-1);
			ArrayList<TreeNode> right = createTree(i+1, high);
			for (int j = 0; j < left.size(); j++) {
				for (int k = 0; k < right.size(); k++) {
					TreeNode node = new TreeNode(i);
					node.left = left.get(j);
					node.right = right.get(k);
					res.add(node);
				}
			}
		}
		return res;
	}
}

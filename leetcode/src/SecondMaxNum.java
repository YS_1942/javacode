/**第二大的数：
输入：
5
1 2 3 4 5
输出：
4 

维持两个最大的数
 * */
import java.util.*;
public class SecondMaxNum {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int n = s.nextInt();
		int max = Integer.MIN_VALUE;
		int sec = Integer.MIN_VALUE;
		for (int i = 0; i < n; i++) {
			int tmp = s.nextInt();
			if(tmp >= max){
				sec = max;
				max = tmp;
			}
			else if(tmp >= sec)
				sec = tmp;
		}
		System.out.println(sec);
	}
}

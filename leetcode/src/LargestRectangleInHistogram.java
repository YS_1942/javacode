
public class LargestRectangleInHistogram {
	public int largestRectangleArea(int[] height) {
        int[] dp = new int[height.length+1];
        int min = Integer.MAX_VALUE;
        for (int i = 1; i <= height.length; i++) {
			min = Math.min(min, height[i-1]);
			dp[i] = Math.max(min*i, Math.min(dp[i-1], dp[i]));
		}
        return dp[height.length];
    }
	public static void main(String[] args) {
		int[] height = new int[]{2,1,5,2,3};
		System.out.println(new LargestRectangleInHistogram().largestRectangleArea(height));
	}
}

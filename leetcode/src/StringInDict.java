import java.util.Set;

/**
 * 给定一个字符串s和一个单词字典dict，确定s是否可以分割成一个或多个字典单词的空格分隔序列。
 * 例如，给定
 * s =“leetcode”，
 * dict=[“leet”，“code”]。
 * 返回true，因为“leetcode”可以分段为“leet code”
 * @author dell
 *
 */
public class StringInDict {
	public boolean wordBreak(String s, Set<String> dict) {
		int len = s.length();
		boolean[] flag = new boolean[len+1];
		flag[0] = true;
		for (int i = 1; i < flag.length; i++) {
			for (int j = 0; j < i; j++) {
				if(flag[j] && dict.contains(s.substring(j, i))){
					flag[i] = true;
					break;
				}
			}
		}
		return flag[len];
	}
}

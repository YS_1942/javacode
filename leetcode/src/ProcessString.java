import java.util.Scanner;
/**头条笔试题，处理字符串，去掉相邻相同的字符*/
public class ProcessString {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int n = s.nextInt();
		ProcessString ps = new ProcessString();
		String[] string = new String[n];
		for (int i = 0; i < n; i++) {
			string[i] = s.nextLine();
			if(i != n-1)
				s.nextLine();
		}
		for (int i = 0; i < n; i++) {
			String ans = ps.help(string[i]);
			if(ans.length() == 0)
				System.out.println("yes");
			else
				System.out.println("no");
		}
	}
	public String help(String str){
		if(str.length() == 0)
			return "";
		char[]cs = str.toCharArray();
		boolean flag = true;
		for (int i = 0; i < cs.length-1; i++) {
			if(i+1<cs.length && cs[i] == cs[i+1]){
				cs[i] = '*';
				cs[i+1] = '*';
				i++;
				flag = false;
			}
		}
		if(flag){
			return str;
		}
		String s ="";
		for (int i = 0; i < cs.length; i++) {
			if(cs[i] != '*')
				s += cs[i];
		}
		return help(s);
	}
}

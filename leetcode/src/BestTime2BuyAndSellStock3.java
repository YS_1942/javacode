/** 
假设您有一个数组，其中第i个元素是第i天给定股票的价格。
设计了一种求最大利润的算法。您最多可以完成两个交易。
注：
您不能同时进行多个交易（即，您必须在再次购买之前出售股票）。
 */
public class BestTime2BuyAndSellStock3 {
	public int maxProfit(int[] prices) {
		int hold1 = Integer.MIN_VALUE;
		int hold2 = Integer.MIN_VALUE;
		int release1 = 0;
		int release2 = 0;
		for (int i : prices) {
			//只卖第2股的最大值
			release2 = Math.max(release2, hold2+i);
			//只买第2股的最大值
			hold2 = Math.max(hold2, release1-i);
			//只卖第1股的最大值
			release1 = Math.max(release1, hold1+i);
			//只买第1股的最大值
			hold1 = Math.max(hold1, -i);
		}
		return release2;
	}
}

/**
 * 使用时间复杂度O(nlogn)、空间复杂度O(1),对单链表进行排序。
 * @author dell
 *
 */
class ListNode{
	int val;
	ListNode next;
	ListNode(int x){
		this.val = x;
		next = null;
	}
}
public class MergeSortLinkedList {
	//快排
	public ListNode sortList(ListNode head) {
        quickSort(head, null);
        return head;
    }
	public void quickSort(ListNode head,ListNode end){
		if(head!=end){
			ListNode partition = partition(head, end);
			quickSort(head, partition);
			quickSort(partition.next, end);
		}
	}
	public ListNode partition(ListNode head,ListNode end){
		ListNode slow = head;
		ListNode fast = head.next;
		while(fast != end){
			if(fast.val < head.val){
				slow = slow.next;
				fast.val = slow.val^fast.val^(slow.val=fast.val);
			}
			fast = fast.next;
		}
		slow.val = head.val ^ slow.val ^(head.val = slow.val);
		return slow;
	}
	//归并
	public ListNode sortList2(ListNode head){
		if(head == null || head.next == null)
			return head;
		ListNode mid = findMid(head);
		ListNode right = sortList2(mid.next);
		mid.next = null;
		ListNode left = sortList2(head);
		return merge(left, right);
	}
	public ListNode findMid(ListNode head){
		ListNode slow = head;
		ListNode fast = head.next;
		while(fast != null && fast.next != null){
			slow = slow.next;
			fast = fast.next.next;
		}
		return slow;
	}
	public ListNode merge(ListNode left,ListNode right){
		if(left == null)
			return right;
		if(right == null)
			return left;
		ListNode tmp = new ListNode(0);
		ListNode head = tmp;
		while(left != null && right != null){
			if(left.val > right.val){
				head.next = right;
				right = right.next;
			}
			else{
				head.next = left;
				left = left.next;
			}
			head = head.next;
		}
		if(left == null)
			head.next = right;
		if(right == null)
			head.next = left;
		return tmp.next;
	}
}

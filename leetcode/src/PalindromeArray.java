/**回文数组，添加一些数，使数组变成回文的，要求添加的数和最小
输入：
8
51 23 52 97 97 76 23 51
输出：
598
 * */
import java.util.*;
public class PalindromeArray {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int n = s.nextInt();
		int[] nums = new int[n];
		for (int i = 0; i < n; i++) {
			nums[i] = s.nextInt();
		}
		int[][] dp = new int[n][n];
		int res = palindromeArrayHelper(nums, dp, 0, n-1);
		System.out.println(res);
	}
	public static int palindromeArrayHelper(int[] nums,int[][] dp,int i,int j){
		if(i>j)
			return 0;
		if(i == j)
			return nums[i];
		if(dp[i][j] != 0)
			return dp[i][j];
		if(nums[i] == nums[j])
			dp[i][j] = 2*nums[i] + palindromeArrayHelper(nums, dp, i+1, j-1);
		else
			dp[i][j] = Math.min(2*nums[i] + palindromeArrayHelper(nums, dp, i+1, j), 2*nums[j]+palindromeArrayHelper(nums, dp, i, j-1));
		return dp[i][j];
	}
}

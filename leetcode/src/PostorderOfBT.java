import java.util.ArrayList;
import java.util.Stack;

/**
 * 给定二叉树，返回其节点值的后序遍历
 * @author dell
 *
 */
public class PostorderOfBT {
	//使用递归
	ArrayList<Integer> result = new ArrayList<>();
	public ArrayList<Integer> postorderTraversal(TreeNode root) {
		sort(root);
		return result;
    }
	public void sort(TreeNode root){
		if(root == null)
			return;
		sort(root.left);
		sort(root.right);
		result.add(root.val);	
	}
	//不使用递归
	//思路：用栈来实现，顶点先入栈，向左到最左下角，保存路径，右不为空，右进站继续左
	//当左右子树都为空时，访问，弹栈，并将pre到p的指针为0，避免下次再访问该路径。
	public ArrayList<Integer> postorderTraversal1(TreeNode root) {
		ArrayList<Integer> result = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();
        Stack<Integer> stack1 = new Stack<>();
        if(root == null)
        	return result;
        stack.push(root);
        while(!stack.isEmpty()){
        	TreeNode cur = stack.pop();
        	stack1.push(cur.val);
        	if(cur.left!=null)
        		stack.push(cur.left);
        	if(cur.right!=null)
        		stack.push(cur.right);
        }
        while(!stack1.isEmpty())
        	result.add(stack1.pop());
        return result;
	}
}

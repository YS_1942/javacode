/**
给定一个包含“x”和“o”的二维板，捕获“x”包围的所有区域。
一个区域是通过将所有的“O”翻转到该包围区域的“X”中来捕获的。
思路：找外圈的O,将它和与其相邻的O改成‘*’，然后将所有的O改成X，再将所有的'*'改成O
 * */
public class SurroundedRegions {
	public void dfs(char[][] board,int row,int col){
		if(row<0||row>(board.length-1)||col <0 ||col>(board[0].length-1)) return;
		if(board[row][col] == 'O'){
			board[row][col] = '*';
			dfs(board, row-1, col);
			dfs(board, row+1, col);
			dfs(board, row, col-1);
			dfs(board, row, col+1);
		
		}
	}
	public void solve(char[][]board){
		if(board.length<=0) return;
		int row = board.length;
		int col = board[0].length;
//		if(row<=2 || col <=2) return;//不知道为什么这样出错
		for (int i = 0; i < row; i++) {
			dfs(board, i, col-1);
			dfs(board, i, 0);
		}
		for (int i = 0; i < col; i++) {
			dfs(board, 0, i);
			dfs(board, row-1, i);
		}
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				if(board[i][j] == 'O') board[i][j] = 'X';
				if(board[i][j] == '*') board[i][j] = 'O';
			}
		}
	}
	public void print(char[][] cs){
		for (int i = 0; i < cs.length; i++) {
			for (int j = 0; j < cs[0].length; j++) {
				System.out.print(cs[i][j]+" ");
			}
			System.out.println();
		}
	}
	public static void main(String[] args) {
		SurroundedRegions sr = new SurroundedRegions();
		char[][] cs = new char[][]{{'X','X','X','X'},{'X','O','O','X'},{'X','X','O','X'},{'X','O','X','X'}};
		sr.print(cs);
		sr.solve(cs);
		System.out.println();
		sr.print(cs);
	}
}

import java.util.Stack;

/**
 * 用反波兰符号计算算术表达式的值。
 * 有效的运算符为+、-、*、/。每个操作数可以是整数或其他表达式。
 * JDK版本不同，可能不能用 == 来判断，只能用equals
 * (用栈实现加减乘除)
 */
public class EvaluateReversePolishNotation {
	public static int evalRPN(String[] tokens) {
		Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < tokens.length; i++) {
        	String str = tokens[i];
        	int a=0,b=0;
        	if(!str.equals("-")&&!str.equals("+") &&!str.equals("*")&&!str.equals("/"))
        		stack.push(Integer.parseInt(str));
        	else{
        		b = stack.pop();
    			a = stack.pop();
    			int sum = 0;
    			if(str.equals("-")) 
    				sum = a - b;
    			else if(str.equals("+")) 
    				sum = a + b;
    			else if(str.equals("*")) 
    				sum = a * b;
    			else if(str.equals("/")) 
    				sum = a / b;
    			stack.push(sum);
        	}
		}
        return stack.pop();
    }
	public static void main(String[] args) {
		String[] tokens = {"4", "13", "5", "/", "+"};
		System.out.println(evalRPN(tokens));
	}
}

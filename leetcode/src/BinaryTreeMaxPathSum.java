/**
给定一个二叉树，找到最大路径和。
路径可以从树中的任何节点开始和结束。
*/
public class BinaryTreeMaxPathSum {
	int maxValue;
	public int maxPathSum(TreeNode root) {
        maxValue = Integer.MIN_VALUE;
        maxPathDown(root);
        return maxValue;
    }
	public int maxPathDown(TreeNode node){
		if(node == null)
			return 0;
		int left = Math.max(0, maxPathDown(node.left));
		int right = Math.max(0, maxPathDown(node.right));
		maxValue = Math.max(maxValue, left+right+node.val);
		return Math.max(left, right)+node.val;
	}
}
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;

/**
输入二叉树，返回二叉树的层序遍历
输入：
    3
   / \
  9  20
    /  \
   15   7
输出：
[
  [3],
  [9,20],
  [15,7]
]
*/
public class LevelorderOfBT {
	public ArrayList<ArrayList<Integer>> levelOrder(TreeNode root) {
		ArrayList<ArrayList<Integer>> res = new ArrayList<>();
		if(root == null)
			return res;
		Queue<TreeNode> queue = new LinkedList<>();
		queue.offer(root);
		while(!queue.isEmpty()){
			ArrayList<Integer> tmp = new ArrayList<>();
			int size = queue.size();
			//这里一定要用一个size取queue的size()，因为queue的长度一直在变！！！
			for (int i = 0; i < size; i++) {
				TreeNode node = queue.poll();
				if(node != null){
					if(node.left != null)
						queue.offer(node.left);
					if(node.right != null)
						queue.offer(node.right);
					tmp.add(node.val);
				}
			}
			res.add(tmp);
		}
		return res;
    }
}

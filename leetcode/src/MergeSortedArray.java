import java.util.Arrays;

/**
给定两个排序整数数组a和b，将b合并到a为一个排序数组。
注：
您可以假定a有足够的空间容纳b中的附加元素。a和b中初始化的元素数分别为m和n。

 */
public class MergeSortedArray {
	public void merge(int A[], int m, int B[], int n) {
        int aIndex = m-1;
        int bIndex = n-1;
        for (int i = m + n - 1; i >= 0; i--) {
        	if(aIndex == -1){
        		A[i] = B[bIndex--];
        		continue;
        	}
        	if(bIndex == -1){
        		A[i] = A[aIndex--];
        		continue;
        	}
			if(A[aIndex] > B[bIndex]){
				A[i] = A[aIndex--];
			}
			else
				A[i] = B[bIndex--];
		}
    }
	public static void main(String[] args) {
		int[] A = new int[]{1,3,5,7,0,0,0,0,0};
		int[] B = new int[]{2,4,6,8};
		new MergeSortedArray().merge(A, 4, B, 4);
		System.out.println(Arrays.toString(A));
	}
}

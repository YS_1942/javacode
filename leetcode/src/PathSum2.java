import java.util.ArrayList;

/**
给定一个二叉树和一个和，找到所有根到叶路径，其中每个路径的和等于给定的和。

*/
public class PathSum2 {
	ArrayList<ArrayList<Integer>> res = new ArrayList<>();
	public ArrayList<ArrayList<Integer>> pathSum(TreeNode root, int sum) {
        ArrayList<Integer> list = new ArrayList<>();
        help(list, root, sum);
        return res;
    }
	public void help(ArrayList<Integer> list,TreeNode node,int sum){
		if(node == null)
			return;
		if(node.left == null && node.right == null && sum - node.val == 0){
			list.add(node.val);
			//这里不能用list，必须要new一个ArrayList！！！！！！！！！！
			res.add(new ArrayList<>(list));
			list.remove(list.size()-1);
			return;
		}
		list.add(node.val);
		help(list, node.left, sum-node.val);
		help(list, node.right, sum-node.val);
		list.remove(list.size()-1);
	}
	
}

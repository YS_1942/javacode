import java.util.ArrayList;
import java.util.Stack;

/**
 * 给定二叉树，返回其节点值的前序遍历
 * @author dell
 *
 */
public class PreorderOfBT {
	ArrayList<Integer> result = new ArrayList<>();
	public ArrayList<Integer> preorderTraversal1(TreeNode root) {
		sort(root);
		return result;
    }
	public void sort(TreeNode root){
		if(root == null)
			return;
		result.add(root.val);	
		sort(root.left);
		sort(root.right);	
	}
	public ArrayList<Integer> preorderTraversal(TreeNode root) {
        ArrayList<Integer> result = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();
        if(root == null)
        	return result;
        stack.push(root);
        while(!stack.isEmpty()){
        	TreeNode cur = stack.pop();
        	result.add(cur.val);
        	if(cur.right!=null)
        		stack.push(cur.right);
        	if(cur.left!=null)
        		stack.push(cur.left);
        }
        return result;
    }
}

/**
给定一个数组，其中元素按升序排序，将其转换为平衡二叉搜索树。
每次将中点值作为root，并将数组分为两部分，递归左右两部分作为数的左右子节点。
*/
public class ConvertSortedArray2BST {
	public TreeNode sortedArrayToBST(int[] num) {
        return toBST(num, 0, num.length-1);
    }
	private TreeNode toBST(int[] num,int head,int tail){
		if(head > tail)
			return null;
		if(head == tail)
			return new TreeNode(num[head]);
		int mid = (head + tail + 1)/2;
		TreeNode root = new TreeNode(num[mid]);
		root.left = toBST(num, head, mid-1);
		root.right = toBST(num, mid+1, tail);
		return root;
	}
}

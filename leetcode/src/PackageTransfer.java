/**包裹运输:
有6*6的包裹；有6种货物，分别为1*1,2*2,3*3...,6*6,.
给定一些货物，求最少需要几个包裹能够装下。
输入：
0 0 4 0 0 1
7 5 1 0 0 0 
0 0 0 0 0 0 （作为结束行）
输出：
2
1
 * */
import java.util.*;
public class PackageTransfer {
	public static void main(String[] args) {
		PackageTransfer ps = new PackageTransfer();
		int[] a = new int[7];
		int sum,num,i;
		//一个包裹放了若干个3*3后，还能放2*2的个数
		int three[] = new int[]{0,5,3,1};
		Scanner s = new Scanner(System.in);
		while(true){
			sum = 0;num =0;
			for (i = 1;  i < 7; i++) {
				a[i] = s.nextInt();
				sum += a[i];
			}
			if(sum == 0) break;
			//计算6*6,5*5,4*4,3*3作为的大箱子数，因为一个包裹它们只能放一个（或者四个3*3）
			num = a[6] + a[5] + a[4] + (a[3]+3)/4;
			//计算还可以填的2*2的空位数，要用已有的2*2来填
			int two_rest = a[4]*5 + three[a[3]%4];
			//如果2*2过多，算的用已有的2*2填充后，剩余的2*2单独需要的箱子数,思考+8
			if(a[2] > two_rest)
				num += (a[2] - two_rest + 8)/9;
			//计算还可以填的1*1的空位数，要用已有的1*1来填
			int one_rest = 36*(num-a[6])-a[5]*25-a[4]*16-a[3]*9-a[2]*4;
			//如果1*1过多，算的用已有的1*1填充后，剩余的1*1单独需要的箱子数
			if(a[1] > one_rest)
				num += (a[1]-one_rest+35)/36;
			System.out.println(num);
		}
	}
}

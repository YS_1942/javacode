import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

/**
输入二叉树，返回二叉树的之字形层序遍历
输入：
    3
   / \
  9  20
    /  \
   15   7
输出：
[
  [3],
  [20,9],
  [15,7]
]
*/
public class LevelorderZigzagOfBT {
	ArrayList<ArrayList<Integer>> res = new ArrayList<>();
	public int depth = 1;
	public ArrayList<ArrayList<Integer>> zigzagLevelOrder(TreeNode root) {
		if(root == null) return res;
		Queue<TreeNode> queue = new LinkedList<>();
		queue.offer(root);
		help(queue);
		return res;
	}
	public void help(Queue<TreeNode> queue){
		if(queue.isEmpty()){
			return;
		}
		int size = queue.size();
		ArrayList<Integer> tmp = new ArrayList<>();
		for (int i = 0; i < size; i++) {
			TreeNode node = queue.poll();
			if(node.left != null)
				queue.offer(node.left);
			if(node.right != null)
				queue.offer(node.right);
			if((depth & 1) != 0){
				tmp.add(node.val);
			}
			else
				tmp.add(0, node.val);
		}
		depth++;
		res.add(tmp);
		help(queue);
	}
}


/**
 * 给出单链表l:l0→l1→…→ln-1→ln，
*重新排序为：L0→LN→L1→LN-1→L 2→LN-2→…
*必须在不更改节点值的情况下就地执行此操作。
*例如，
*给定1,2,3,4，将其重新排序为1,4,2,3
 * @author dell
 *
 */
public class ReorderLinkedList {
	public void reorderList(ListNode head) {
		if(head == null || head.next == null)
			return;
		ListNode slow = head,fast = head;
		while(fast.next != null && fast.next.next != null){
			fast = fast.next.next;
			slow = slow.next;
		}
		ListNode after = slow.next;
		slow.next = null;
		ListNode pre = null;
		while(after!=null){
			ListNode tmp = after.next;
			after.next = pre;
			pre = after;
			after = tmp;
		}
		ListNode first = head;
		after = pre;
		while(first != null &&after != null){
			ListNode ftmp = first.next;
			ListNode aftmp = after.next;
			first.next = after;
			first = ftmp;
			after.next = first;
			after = aftmp;
		}
    }
}

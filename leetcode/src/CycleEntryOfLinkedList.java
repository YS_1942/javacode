import java.util.HashSet;

/**
 * 给定一个链接列表，返回循环开始的节点。如果没有循环，返回空
 * 跟进：你能在不使用额外空间的情况下解决它吗？
 * @author dell
 *
 */
public class CycleEntryOfLinkedList {
	//用辅助空间
	public ListNode detectCycle(ListNode pHead) {
		HashSet<ListNode> set = new HashSet<>();
		while(pHead != null){
			if(set.contains(pHead))
				return pHead;
			else{
				set.add(pHead);
				pHead = pHead.next;
			}
		}
		return null;
    }
	//不用辅助空间
	public ListNode detectCycle1(ListNode pHead) {
		ListNode slow = pHead;
		ListNode fast = pHead;
		while(fast!=null && fast.next !=null){
			fast = fast.next.next;
			slow = slow.next;
			if(fast == slow)
				break;
		}
		if(fast == null || fast.next == null)
			return null;
		fast = pHead;
		while(fast != slow){
			fast = fast.next;
			slow = slow.next;
		}
		return fast;
	}
}

import java.util.ArrayList;

/**
格雷码是一个二进制数字系统，其中两个连续值仅在一个位上不同。
给定一个非负整数n，表示代码中的位总数，打印格雷码的序列。格雷码序列必须以0开头。
例如，给定n=2，返回[0,1,3,2]。其格雷码序列为：
00 - 0
01 - 1
11 - 3
10 - 2
对于给定的n，格雷码序列不是唯一定义的。
例如，根据上述定义，[0,2,3,1]也是有效的格雷码序列。
*/
public class GrayCode {
	public ArrayList<Integer> grayCode(int n) {
		ArrayList<Integer> res = new ArrayList<>();
        if(n < 1)
        	return res;
        res.add(0);
        res.add(1);
        if(n == 1)
        	return res;
        help(2, n,res);
//        return new ArrayList<>(res);
        return res;
    }
	public void help(int i,int n,ArrayList<Integer> res){
		if(i > n)
			return;
		int size = res.size();
		for (int j = 0; j < size; j++) {
			res.add(res.get(size-1-j)+ (1<<(i-1)));
		}
		help(i+1, n,res);
	}
	//牛客网
	public ArrayList<Integer> grayCode1(int n) {
		ArrayList<Integer> res = new ArrayList<>();
		int num = 1<<n;
		for (int i = 0; i < num; i++) {
			res.add(i>>1^i);
		}
		return res;
	}
	public static void main(String[] args) {
		GrayCode g = new GrayCode();
		int n = 5;
		for (int i = 2; i <= n; i++) {
			System.out.println(g.grayCode(i).toString());
			System.out.println(g.grayCode1(i).toString());
		}
	}
}

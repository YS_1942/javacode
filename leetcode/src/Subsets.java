import java.util.ArrayList;
import java.util.Arrays;

/**
给定一个可能包含重复项的整数集合，s返回所有可能的子集。
注：
子集中的元素必须按非降序排列。
解决方案集不能包含重复的子集。
输入：
S =[1,2,2]
输出：
[
  [2],
  [1],
  [1,2,2],
  [2,2],
  [1,2],
  []
]
*/
public class Subsets {
	ArrayList<ArrayList<Integer>> listAll = new ArrayList<>();
	public ArrayList<ArrayList<Integer>> subsetsWithDup(int[] num) {
		if(num == null || num.length == 0)
			return listAll;
		ArrayList<Integer> list = new ArrayList<>();
		Arrays.sort(num);
		
		FindSubset(num,0,list);
		return listAll;
	}
	public void FindSubset(int[] num,int start,ArrayList<Integer> list){
		listAll.add(new ArrayList<>(list));
		for (int i = start; i < num.length; i++) {
			if(i > start && num[i] == num[i-1])
				continue;
			list.add(num[i]);
			FindSubset(num, i+1, list);
			list.remove(list.size()-1);
		}
	}
}

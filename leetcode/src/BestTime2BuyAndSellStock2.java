/**
假设您有一个数组，其中第i个元素是第i天给定股票的价格。
设计了一种求最大利润的算法。您可以完成任意数量的交易
（即，购买一份股票,出售一份股票多次）。
但是，您不能同时进行多个交易（即，您必须在再次购买之前出售股票）。
*/
public class BestTime2BuyAndSellStock2 {
	/**
	 * 直接统计相邻的增量即可，因为第i天可以卖一次，也可以买一次
	 * */
	public int maxProfit(int[] prices) {
        if(prices == null || prices.length == 0)
        	return 0;
		int sum = 0;
		for (int i = 0; i < prices.length-1; i++) {
			if(prices[i]<prices[i+1])
				sum += prices[i+1] - prices[i];
		}
        return sum;
    }
}

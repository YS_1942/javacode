import java.util.ArrayList;

/**
给定一个三角形，求从上到下的最小路径和。每一步都可以移动到下面行的相邻数字。
例如，给定以下三角形
[
     [2],
    [3,4],
   [6,5,7],
  [4,1,8,3]
]
 从上到下的最小路径和为11（即2+3+5+1=11）。
O(n)的额外空间复杂度（n是行数）
 */
public class Triangle {
	//从下往上，上一层选下一层相邻较小的那一个，逐渐修改值
	public int minimumTotal(ArrayList<ArrayList<Integer>> triangle) {
		ArrayList<Integer> res = triangle.get(triangle.size()-1);
		for (int i = triangle.size()-2; i >= 0 ; i--) {
			for (int j = 0; j < triangle.get(i).size(); j++) {
				res.set(j, triangle.get(i).get(j) + Math.min(res.get(j),res.get(j+1)));
			}
		}
		return res.get(0);
	}
}
